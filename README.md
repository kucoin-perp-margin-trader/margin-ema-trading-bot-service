# Margin EMA Trading Bot Service

Service that trades based on the EMA-12 and EMA-26 crossing signals. Either take 1min with low leverage or 5min with higher leverage (or both).
