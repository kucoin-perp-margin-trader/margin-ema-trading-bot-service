var express = require('express');
var router = express.Router();

const axios = require('axios')
const config = require('../kucoin-config')

const ta = require('ta.js')

//granularities = array of timeframes (1candle 5min)
const main = async(symbols, granularity) => {
  
      const curr_date_milis = Math.floor(new Date().getTime()/1000)

      var res = []

      for(const symbol of symbols) {
        //get little more than 26
          const time_interval_back = curr_date_milis - granularity*60*35
          const candle_data = await fetchCandleData(symbol, granularity, curr_date_milis, time_interval_back)
          const candle_closes = []
          candle_data.forEach(elem => candle_closes.push(elem[2]))

          const candle_closes_last_st = getCCLast(candle_closes)
          const candle_closes_last = candle_closes_last_st.map(i=>Number(i))
          //last candles (without newest arg)
          console.log(candle_closes_last)
          const ema_12_lc = ta.ema(candle_closes_last, 12)[0]
          const ema_26_lc = ta.ema(candle_closes_last, 26)[0]

          const candle_closes_current = getCCCurrent(candle_closes)
          //current candles (without oldest arg)
          const ema_12_cc = ta.ema(candle_closes_current, 12)[0]
          const ema_26_cc = ta.ema(candle_closes_current, 26)[0]
          console.log(ema_12_cc)
          console.log(ema_26_cc)
          const ema_signal = getEMASignal(ema_12_lc, ema_12_cc, ema_26_lc, ema_26_cc)

          const obj = {
            symbol,
            granularity,
            ema_signal,
            lastPrice: candle_closes[candle_closes.length-1]
          }
          res.push(obj)
      } 
      return res
}

const fetchCandleData = async(symbol, granularity, curr_date_milis, time_interval_back) => {
  //this gets you the "candles" of the kucoin perps on https://futures.kucoin.com/trade/ETHUSDTM, NOT THE CANDLES SHOWN THERE (but the line)..very similar but not the same
  var ticker = symbol.slice(0, -5)+'-USDT'
  if(ticker == 'XBT-USDT') ticker = 'BTC-USDT'
  //`${config.baseUrl}/api/v1/kline/query?symbol=${symbol}&granularity=${granularity}&from=${time_interval_back}&to=${curr_date_milis}`
  const data = await axios.get(`https://api.kucoin.com/api/v1/market/candles?type=${granularity}min&symbol=${ticker}&startAt=${time_interval_back}&endAt=${curr_date_milis}`)
       .then(res => {
         return(res.data.data)
       })
       .catch(err => {
         console.error(err)
       })
  return data     
}

const getEMA = (candle_closes, days) => {
  if(candle_closes.length==0 || days==0) return 0
  var k = 2/(days + 1);
  const adjusted_cc = candle_closes.slice(Math.max(candle_closes.length - days, 0))
  // first item is just the same as the first item in the input
  var emaArray = [adjusted_cc[0]];
  // for the rest of the items, they are computed with the previous one
  for (var i = 1; i < adjusted_cc.length; i++) {
    emaArray.push(adjusted_cc[i] * k + emaArray[i - 1] * (1 - k));
  }
  return emaArray.pop();
}

const getEMASignal = (ema_12_yesterday, ema_12, ema_26_yesterday, ema_26) => {
  if(ema_12_yesterday==0 || ema_12==0 || ema_26_yesterday==0 || ema_26==0) return 0
    const ema_today_diff = ema_12 - ema_26
    const ema_yesterday_diff = ema_12_yesterday - ema_26_yesterday
    //2=buy, 1=hold(do nothing), 0=sell
    if(ema_today_diff > 0 && ema_yesterday_diff < 0) {
      return 2
    } else if (ema_today_diff < 0 && ema_yesterday_diff > 0) {
      return 0
    } else {
      return 1
    }
  }

const getCCLast = candle_closes => {
  return candle_closes.slice(0, -1)
}  

const getCCCurrent = candle_closes => {
  let shifted = candle_closes.shift()
  return candle_closes
}

router.get('/', async(req, res) => {
  if (
    req.query.symbols == undefined ||
    req.query.granularity == undefined
  ) {
    res.status(400).send();
    return
  }
  try {
    const data = await main(req.query.symbols, req.query.granularity)
    res.status(200).send(data)
  } catch (err) {
    //500
    console.error(err)
  }
});

module.exports = router;