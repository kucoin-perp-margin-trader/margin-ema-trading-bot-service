module.exports = {
    baseUrl:'https://api-futures.kucoin.com',
    apiAuth: {
        key: process.env.key,
        secret: process.env.secret,
        passphrase: process.env.passphrase,
        timestamp: Date.now(),
        version: 2,
    },
}